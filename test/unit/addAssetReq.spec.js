import AddAssetReq from '../../src/addAssetReq';
import dummy from '../dummy';

/*
 test methods
 */
describe('AddAssetReq class', () => {
    describe('constructor', () => {
        it('throws if accountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddAssetReq(
                        null,
                        dummy.productLineId,
                        dummy.assetSerialNumber,
                        dummy.assetDescription
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'accountId required');
        });
        it('sets accountId', () => {
            /*
             arrange
             */
            const expectedAccountId = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new AddAssetReq(
                    expectedAccountId,
                    dummy.productLineId,
                    dummy.assetSerialNumber,
                    dummy.assetDescription
                );

            /*
             assert
             */
            const actualAccountId = objectUnderTest.accountId;
            expect(actualAccountId).toEqual(expectedAccountId);
        });
        it('throws if productLineId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddAssetReq(
                        dummy.accountId,
                        null,
                        dummy.assetSerialNumber,
                        dummy.assetDescription
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'productLineId required');
        });
        it('sets productLineId', () => {
            /*
             arrange
             */
            const expectedProductLineId = dummy.productLineId;

            /*
             act
             */
            const objectUnderTest =
                new AddAssetReq(
                    dummy.accountId,
                    expectedProductLineId,
                    dummy.assetSerialNumber,
                    dummy.assetDescription
                );

            /*
             assert
             */
            const actualProductLineId = objectUnderTest.productLineId;
            expect(actualProductLineId).toEqual(expectedProductLineId);
        });
        it('throws if serialNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddAssetReq(
                        dummy.accountId,
                        dummy.productLineId,
                        null,
                        dummy.assetDescription
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'serialNumber required');
        });
        it('sets serialNumber', () => {
            /*
             arrange
             */
            const expectedSerialNumber = dummy.assetSerialNumber;

            /*
             act
             */
            const objectUnderTest =
                new AddAssetReq(
                    dummy.accountId,
                    dummy.productLineId,
                    expectedSerialNumber,
                    dummy.assetDescription
                );

            /*
             assert
             */
            const actualSerialNumber = objectUnderTest.serialNumber;
            expect(actualSerialNumber).toEqual(expectedSerialNumber);
        });
        it('does not throw if description is null', () => {
            /*
             arrange
             */
            new AddAssetReq(
                dummy.accountId,
                dummy.productLineId,
                dummy.assetSerialNumber,
                null
            );
        });
        it('sets description', () => {
            /*
             arrange
             */
            const expectedDescription = dummy.assetDescription;

            /*
             act
             */
            const objectUnderTest =
                new AddAssetReq(
                    dummy.accountId,
                    dummy.productLineId,
                    dummy.assetSerialNumber,
                    expectedDescription
                );

            /*
             assert
             */
            const actualDescription = objectUnderTest.description;
            expect(actualDescription).toEqual(expectedDescription);
        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /*
             arrange
             */
            const objectUnderTest =
                new AddAssetReq(
                    dummy.accountId,
                    dummy.productLineId,
                    dummy.assetSerialNumber,
                    dummy.assetDescription
                );

            const expectedObject =
            {
                accountId: objectUnderTest.accountId,
                productLineId: objectUnderTest.productLineId,
                serialNumber: objectUnderTest.serialNumber,
                description: objectUnderTest.description
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);

        });
    });
});
