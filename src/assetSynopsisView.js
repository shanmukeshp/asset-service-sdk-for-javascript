export default class AssetSynopsisView {

    /*
     fields
     */
    _id:string;

    _productGroupId:number;

    _productGroupName:string;

    _productLineId:number;

    _productLineName:string;

    _serialNumber:string;

    _description:string;

    /**
     *
     * @param {string} id
     * @param {number} productGroupId
     * @param {string} productGroupName
     * @param {number} productLineId
     * @param {string} productLineName
     * @param {string} serialNumber
     * @param {string} description
     */
    constructor(id:string,
                productGroupId:number,
                productGroupName:string,
                productLineId:number,
                productLineName:string,
                serialNumber:string,
                description:string) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        this._productGroupId = productGroupId;

        this._productGroupName = productGroupName;

        if (!productLineId) {
            throw new TypeError('productLineId required');
        }
        this._productLineId = productLineId;

        this._productLineName = productLineName;

        if (!serialNumber) {
            throw new TypeError('serialNumber required');
        }
        this._serialNumber = serialNumber;

        this._description = description;

    }

    /**
     * @returns {string}
     */
    get id():string {
        return this._id;
    }

    /**
     * @returns {number}
     */
    get productGroupId():number {
        return this._productGroupId;
    }

    /**
     * @returns {string}
     */
    get productGroupName():string {
        return this._productGroupName;
    }

    /**
     * @returns {number}
     */
    get productLineId():number {
        return this._productLineId;
    }

    /**
     * @returns {string}
     */
    get productLineName():string {
        return this._productLineName;
    }

    /**
     * @returns {string}
     */
    get serialNumber():string {
        return this._serialNumber;
    }

    /**
     * @returns {string}
     */
    get description():string {
        return this._description;
    }

    toJSON(){
        return{
            assetId:this._id,
            productGroupId:this._productGroupId,
            productGroupName:this._productGroupName,
            productLineId:this._productLineId,
            productLineName:this._productLineName,
            serialNumber:this._serialNumber,
            description:this._description
        }
    }
}